/*Inside of routes.js, create an exportable function that will handle 
different endpoints for our server:

For the "/" endpoint, display a message saying "This is the homepage"

For the "/login" endpoint, display a message saying "This is the login page."

for about "/about" endpoint, display a message saying "This is the about page."

for any other page, display a message saying "Page not found" with a 404 status code

Import that function into index.js and add it to the createServer() method


*/

const http = require("http");


module.exports.requestListener = function (request,response){
	response.writeHead(200, {'Content-Type': 'text/plain'})
	

	response.end('Hello Jino') 

}

const server = http.createServer(requestListener)

const port = 4000


server.listen(port) 

console.log(`Server Running at port ${port}`) 

