/*
const http = require("http");




const server = http.createServer(function (request,response){
	//When a browser tries to contact a server, it sends a request
	//That request includes many different kinds of information
	//Among that information is the endpoint that the browser wants to access


	if(request.url === `/greeting`){
			response.writeHead(200, {'Content-Type': 'text/plain'})

			response.end('Hello Jino')
	}else if(request.url === `/`){
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('This is the homepage')
	}else{
		response.writeHead(404, {'Content-Type': 'text/plain'} )

		response.end('Page not available')
	}


})

const port = 4000


server.listen(port)

console.log(`Server Running at port ${port}`)
*/


module.exports.homePage = function (request,response){
	response.writeHead(200, {'Content-Type': 'text/plain'})
	
	response.end('This is the homepage')

}

module.exports.logInPage = function (request,response){
	response.writeHead(200, {'Content-Type': 'text/plain'})
	
	response.end('This is the login page')

}

module.exports.aboutPage = function (request,response){
	response.writeHead(200, {'Content-Type': 'text/plain'})
	
	response.end('This is the about page')

}

module.exports.pageNotAvailable = function (request,response){
	response.writeHead(404, {'Content-Type': 'text/plain'} )

	response.end('Page not available')
}